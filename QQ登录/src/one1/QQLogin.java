package one1;
import java.awt.*;
import javax.swing.*;
/**
 * @author 万里只寻你
 */

public class QQLogin extends JFrame{
    private static final JFrame jf=new JFrame();
    public static void initLogin() {
        jf.setSize(426,300);//设置窗口尺寸
        jf.setLocation(497,242);//设置窗口在屏幕显示位置
        jf.setUndecorated(true);//设置JFrame窗口边框不显示
        jf.setResizable(false);//禁止改变窗口大小
        //2.根据QQ登录界面效果，进行布局分配
        BorderLayout border_layout=new BorderLayout();
        jf.setLayout(border_layout);
        //2.1创建并加入顶部面板
        JPanel panel_north=CreatePanel.CreateNorthPanel(jf);
        jf.add(panel_north,BorderLayout.PAGE_START);
        //2.2创建并加入中部面板
        JPanel panel_center=CreatePanel.CreateCenterPanel(jf);
        jf.add(panel_center,BorderLayout.CENTER);
        //2.3创建并加入左侧面板
        JPanel panel_west=CreatePanel.CreateWestPanel();
        jf.add(panel_west,BorderLayout.LINE_START);
        //2.4创建并加入底部面板
        JPanel panel_south=CreatePanel.CreateSouthPanel();
        jf.add(panel_south,BorderLayout.PAGE_END);
        //2.5创建并加入右侧面板
        JPanel panel_east=CreatePanel.createEastPanel();
        jf.add(panel_east,BorderLayout.LINE_END);
        jf.setVisible(true); //设置窗口可见
    }
    public static void main(String[] args) {
        //使用SwingUtilities工具类调用createAndShowGUI()方法并显示GUI程序
        SwingUtilities.invokeLater(QQLogin::initLogin);
    }
}