package one1;

import java.awt.*;
import javax.swing.*;
public class CreatePanel {
    private static LoginListener ll=null;
    //顶部面板区域
    public static JPanel CreateNorthPanel (JFrame jf) {
        //创建一个JPanel顶部面板
        JPanel panel=new JPanel();
        //取消面板内默认布局
        panel.setLayout(null);
        //设置顶部面板尺寸
        panel.setPreferredSize(new Dimension(0,140));
        //1.1向顶部面板添加背景照片
        ImageIcon image=new ImageIcon("D:\\GUL图形用户接口\\QQ登录\\src\\image\\back.jpg");
        JLabel background=new JLabel(image);
        //设置背景照片的位置及尺寸
        background.setBounds(0,0,426,image.getIconHeight());
        panel.add(background);
        //1.2在顶部JPanel面板右上角添加一个退出按钮
        JButton out=new JButton(new ImageIcon("D:\\GUL图形用户接口\\QQ登录\\src\\image\\close2_normal.jpg"));
        out.setBounds(403,0,26,26);
        //设置鼠标移动到退出按钮时更改图片
        out.setRolloverIcon(new ImageIcon("D:\\GUL图形用户接口\\QQ登录\\src\\image\\close2_hover.jpg"));
        //取消按钮边框效果
        out.setBorderPainted(false);
        out.addActionListener(event->jf.dispose());
        panel.add(out);
        return panel;
    }
    //左侧面板区域
    public static JPanel CreateWestPanel() {
        //2.创建一个JPanel左侧面板
        JPanel panel=new JPanel();
        panel.setLayout(null);
        panel.setPreferredSize(new Dimension(130,0));
        //向左侧面板添加背景照片
        ImageIcon image=new ImageIcon("D:\\GUL图形用户接口\\QQ登录\\src\\image\\qq.jpg");
        JLabel background=new JLabel(image);
        background.setBounds(0,0,120,110);
        panel.add(background);
        return panel;
    }
    //中部面板区域
    public static JPanel CreateCenterPanel(JFrame jf) {
        //3.创建一个JPanel中部面板
        JPanel panel=new JPanel();
        panel.setLayout(null);
        //3.1创建一个JcomboBox下拉框组件，并初始化qq账号
        String[] str = {"123456789","987654321","1314520888"};
        JComboBox<Object>jcoCenter= new JComboBox<>( str );
        panel.add(jcoCenter);
        //设置下拉框可编辑
        jcoCenter.setEditable(true);
        jcoCenter.setBounds(0,15,175,30);
        //设置下拉框内容字体
        jcoCenter.setFont(new Font("Calibri", Font.PLAIN,13));
        //3.2创建一个JPasswordField密码框组件
        JPasswordField jPaCenter=new JPasswordField();
        //设置密码框面板为FlowLayout布局
        jPaCenter.setLayout(new FlowLayout(FlowLayout.RIGHT,0,0));
        jPaCenter.setBounds(0,44,175,30);
        jPaCenter.setPreferredSize(new Dimension(185,25));
        panel.add(jPaCenter);
        //3,3创建ImageIcon小键盘图标组件，并加入到密码框组件中
        ImageIcon image=new ImageIcon("D:\\GUL图形用户接口\\QQ登录\\src\\image\\keyboard.jpg");
        JButton jbu=new JButton(image);
        jbu.setPreferredSize(new Dimension(22,20));
        jbu.setBorderPainted(false);
        jPaCenter.add(jbu);
        //3.4创建两个JCheckBox多选框组件
        JCheckBox jch1=new JCheckBox("记住密码");
        //设置选中时不显示边框
        jch1.setFocusPainted(false);
        jch1.setFont(new Font("宋体", Font.PLAIN,13));
        jch1.setBounds(100,85,80,20);
        panel.add(jch1);
        JCheckBox jch2=new JCheckBox("自动登录");
        jch2.setFocusPainted(false);
        jch2.setFont(new Font("宋体", Font.PLAIN,12));
        jch2.setBounds(100,85,80,20);
        panel.add(jch2);
        ll=new LoginListener(jcoCenter,jPaCenter,jf);
        return panel;
    }
    //右侧面板区域
    public static JPanel createEastPanel() {
        //4.创建一个JPanel右侧面板
        JPanel panel=new JPanel();
        panel.setLayout(null);
        panel.setPreferredSize(new Dimension(100,0));
        //创建两个JLable标签组件
        JLabel regist=new JLabel("注册账号");
        regist.setForeground(new Color(100,149,238));
        regist.setBounds(0,13,60,30);
        regist.setFont(new Font("宋体", Font.PLAIN,12));

        JLabel regetpwd=new JLabel("找回密码");
        regist.setForeground(new Color(100,149,238));
        regist.setBounds(0,43,60,30);
        regist.setFont(new Font("宋体", Font.PLAIN,12));
        panel.add(regist);
        panel.add(regetpwd);
        return panel;
    }
    //底部面板区域
    public static JPanel CreateSouthPanel() {
        //6.创建一个JPanel底部面板
        JPanel panel=new JPanel();
        panel.setPreferredSize(new Dimension(0,51));
        panel.setLayout(null);
        //6.1创建左下角多人登陆图标组件
        JButton jble=new JButton(new ImageIcon("D:\\GUL图形用户接口\\QQ登录\\src\\image\\single_normal.jpg"));
        jble.setPreferredSize(new Dimension(40,40));
        jble.setFocusPainted(false);
        jble.setRolloverIcon(new ImageIcon("D:\\GUL图形用户接口\\QQ登录\\src\\image\\single_down.jpg"));
        jble.setBorderPainted(false);
        //设置不显示按钮区域
        jble.setContentAreaFilled(false);
        jble.setBounds(0,10,40,40);
        jble.setToolTipText("多账号登录");
        //6.2创建底部中间登录图标组件
        ImageIcon image=new ImageIcon("D:\\GUL图形用户接口\\QQ登录\\src\\image\\login_normal.jpg");
        JButton jb=new JButton("登   录",image);//
        jb.setFont(new Font("宋体", Font.PLAIN,13));
        jb.setBounds(130,0,175,40);
        //将文字放在图片中间
        jb.setHorizontalTextPosition(SwingConstants.CENTER);
        jb.setFocusPainted(false);
        jb.setContentAreaFilled(false);
        jb.setBorderPainted(false);
        jb.setRolloverIcon(new ImageIcon("D:\\GUL图形用户接口\\QQ登录\\src\\image\\login_hover.jpg"));
        jb.addActionListener(ll);
        //6.3创建右下角二维码登录图标组件
        JButton jbri=new JButton(new ImageIcon("D:\\GUL图形用户接口\\QQ登录\\src\\image\\right_normal.jpg"));
        jbri.setBounds(380,10,40,40);
        jbri.setFocusPainted(false);
        jbri.setBorderPainted(false);
        jbri.setContentAreaFilled(false);
        jbri.setRolloverIcon(new ImageIcon("D:\\GUL图形用户接口\\QQ登录\\src\\image\\right_hover.jpg"));
        jbri.setToolTipText("二维码登录");
        //将底部3个组件添加到底部JPanel面板中
        panel.add(jble);
        panel.add(jb);
        panel.add(jbri);
        return panel;

    }
}
